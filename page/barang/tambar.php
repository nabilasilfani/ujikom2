<?php 
include "koneksi.php";
include "kode_peminjaman.php";
session_start();
if (isset($_SESSION['username'])){
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Admin</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

  </head>
  <body>
   <nav>
    <div class="container">
      <div class="nav-wrapper">
        <div class="brand-logo">INVENSCOO</div>
        <ul class="right hide-on-med-and-down">
          <li><a href="logout2.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
        </ul>
      </div>
    </div>
  </nav>
    <div class="container">
    <div class="row">

      <div class="col s3">
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header" style="color:#039be5;"><a href="index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
            <div class="collapsible-body"><a href="databarang.php">Data Barang</a></div>
            <div class="collapsible-body"><a href="datajenis.php">Data Jenis</a></div>
            <div class="collapsible-body"><a href="dataruang.php">Data Ruang</a></div>
          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
            <div class="collapsible-body"><a href="petugas.php">Data Petugas</a></div>
            <div class="collapsible-body"><a href="pegawai.php">Data Pegawai</a></div>

          </li>
          <li>
            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
            <div class="collapsible-body"><a href="datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
            <div class="collapsible-body"><a href="datapengembalian.php">Data pengembalian</a></div>
          </li>
        </ul>
      </div>
    
  
      <div class="col s9">
        <div class="card">
          <div class="card-heading"><h5>Tambah Data Barang</h5></div>
          <div class="card-content">
            <form action="" method="post">
              <label>Nama</label>
              <input type="hidden" name="id_petugas" value="<?php echo $_SESSION['id_petugas'] ?>" placeholder="">
              <input type="text" name="nama" placeholder="Masukkan Nama" required="" /><br>
              <label>Kondisi</label> 
              <select name="kondisi" required="" />
                  <option disabled selected>Pilih Kondisi</option>
                  <option value=" Sanagt Baik">Sangat Baik</option>
                  <option value="Baik">Baik</option>
                  <option value="Rusak">Rusak</option>
              </select><br>
              <label>Jumlah</label>
              <input type="text" name="jumlah" placeholder="Masukkan Jumlah" required="" /><br>
              <label>Tanggal Register</label>
              <input type="date" name="tanggal_register" placeholder="Masukkan Tanggal Register" required="" /><br>
              <label>Id Ruang</label>
              <select name="id_ruang" required="" />
                  <option disabled selected>Pilih Ruang</option>
                  <?php   
                      $sql=mysql_query("SELECT * FROM ruang");
                      while($data=mysql_fetch_array($sql)){
                   ?>
                   <option value="<?php echo $data['id_ruang']?>"><?php echo $data['nama_ruang']; ?></option>
                   <?php
                      }
                    ?>
              </select><br>
              <!-- <input type="text" name="id_ruang" placeholder="Masukkan Id Ruang" required="" /><br> -->
              <label>Kode Inventaris</label>
              <input type="text" name="kode_inventaris" placeholder="Masukkan Kode Inventaris" required="" /><br>
              <input type="submit" name="simpan" class="btn btn-success" value="simpan" />
              <input type="send" name="cancel" class="btn btn-primary" value="Cancel" />
            <?php
            include"koneksi.php";
            if(isset($_POST['simpan']))
            {

              $nama = $_POST['nama'];
              $kondisi = $_POST['kondisi'];
              $jumlah = $_POST['jumlah'];
              $tanggal_register = $_POST['tanggal_register'];
              $id_ruang = $_POST['id_ruang'];
              $kode_inventaris = $_POST['kode_inventaris'];
              $input = mysql_query("INSERT INTO inventaris (nama,kondisi,jumlah,tanggal_register,id_ruang,kode_inventaris) VALUES ('$nama','$kondisi','$jumlah','$tanggal_register','$id_ruang','$kode_inventaris')");
              if($input){
                echo"<script>window.location.assign('databarang.php')</script>";
              }else{
                echo"Gagal";
              }
            }
            ?>
          </form>
        </div>
        </div>
      </div>

</div>
</div>
</div>


<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l4 s12" style="width: 32.333333%; font-size: 16px;">
        <h5 class="white-text">Hubungi Kami</h5>
        <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
        <p><i class="material-icons" style="width: 27px; color: white">website</i><a style="color:white">www.smkn1ciomas.sch.id</a> </p>
        <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white">telephone</i><a style="color:white">08147919696594 </a></p>
      </div>

      <div class="col l4 s12" style="overflow: hidden; margin-left: 320px; font-size: 16px;">
        <h5 class="white-text">About</h5>
        <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siapapun dalam mengetahui informasi inventaris yang ada di sekolah dan memudahkan staf tata usaha dalam merekap inventaris yang ada di sekolah.</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © SMK Negeri 1 CIOMAS
    </div>
  </div>
</footer>

</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
  <script type="text/javascript">
        $(document).ready(function() {
          $('select').material_select();
        });
      </script> 
