<?php  
 include "koneksi.php";
$output = '';
if(isset($_POST["export"]))
{
 $result = $koneksi->query("SELECT * FROM inventaris");
 if($result->num_rows > 0)
 {
  $output .= '
   <table class="table" border="1">  
                    <tr>  
                         <th>Kode Inventaris</th>  
                         <th>Nama Barang</th>  
                         <th>Barang</th>
                         <th>Kondisi</th>
                         <th>Jumlah</th>  
                         <th>Jenis</th>  
                         <th>Ruang</th>
                         <th>Tanggal Register</th>
                         <th>Keterangan</th>
                         <th>Petugas</th>
                    </tr>
  ';
  while($row = $result->fetch_array())
  {
   $output .= '
  <tr>  
     <td>'.$row["kode_inventaris"].'</td>  
     <td>'.$row["nama_barang"].'</td>  
     <td>'.$row["asal_barang"].'</td>  
     <td>'.$row["kondisi"].'</td>
     <td>'.$row["jumlah"].'</td>
     <td>'.$row["jenis"].'</td>
     <td>'.$row["ruang"].'</td>
     <td>'.$row["tanggal_register"].'</td>
     <td>'.$row["keterangan"].'</td>
     <td>'.$row["petugas"].'</td>
    </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Laporan Inventaris.xls');
  echo $output;
 }
}
?>