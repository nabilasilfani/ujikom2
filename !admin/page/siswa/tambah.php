<?php 
include "../../koneksi.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Absensi</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../../../dist/css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="../../../style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <!-- DataTables -->
    <link rel="stylesheet" href="dist/plugin/datatables/dataTables.bootstrap.css">
    <script type="text/javascript" src="../../../dist/js/jquery.js"></script>
    <script type="text/javascript" src="../../../dist/js/materialize.min.js"></script>
    <script src="../../../dist//plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../../dist/plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    
    <script type="text/javascript">
      $(function () {
        $('#table').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": false,
          "bInfo": true,
          "bAutoWidth": true
        });
      });
    </script>

</head>
<body>
	  <nav>
    <div class="container">
      <div class="nav-wrapper">
        <div class="brand-logo">INVENSCOO</div>
        <ul class="right hide-on-med-and-down">
          <li><a href="../../logout.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Navbar goes here -->
  <div class="container">
  <div class="row">
    <div class="col s3">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header" style="color:#039be5;"><a href="../../index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
          <div class="collapsible-body"><a href="../barang/databarang.php">Data Barang</a></div>
          <div class="collapsible-body"><a href="../jenis/datajenis.php">Data Jenis</a></div>
          <div class="collapsible-body"><a href="../ruang/dataruang.php">Data Ruang</a></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
            <div class="collapsible-body"><a href="../petugas/datasiswa.php">Data Siswa</a></div>
            <div class="collapsible-body"><a href="../pegawai/pegawai.php">Data Guru</a></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
             <div class="collapsible-body"><a href="../../datapeminjaman.php">Data Peminjaman</a></div>
            <!-- <div class="collapsible-body"><a href="../../datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div> -->
            <div class="collapsible-body"><a href="../../datapengembalian.php">Data pengembalian</a></div>
        </li>
      </ul>
    </div>
      <div class="col s12 m8 l8"> 
        <div class="card">
          <div class="card-heading"><h5>Tambah Siswa</h5></div>
          <div class="card-content">

           <form action="" method="post">
            <label>Nis</label>
            <input type="text" name="nis"  placeholder="Nis"/></p>
            <label>Nama</label>
            <input type="text" name="nama" placeholder="Nama"/><br></p>
            <label>Kelas</label>
            <input type="text" name="kelas" placeholder="Kelas"/><br></p>
            <label>Jurusan</label>
            <!--<input type="text" class="form-control" name="jurusan" placeholder="Jurusan"/><br></p>-->
            <select name="jurus">
              <option disabled selected>Pilih Jurusan</option>
              <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
              <option value="Animasi">Animasi</option>
              <option value="Teknik  Kendaraan Ringan">Teknik Kendaraan Ringan</option>
              <option value="Teknik Pengelasan">Teknik Pengelasan</option>
            </select>
            <input type="submit" name="submit" class="btn btn-success" value="Submit" />
            <input type="submit" name="cancel" class="btn btn-primary" value="Cancel" />
          </form>
          <?php
          if(isset($_POST['submit']))
          {

            $nis = $_POST['nis'];
            $nama = $_POST['nama'];
            $kelas = $_POST['kelas'];
            $jurus = $_POST['jurus'];
            $input = mysql_query("INSERT INTO siswa (nis,nama,kelas,jurusan) VALUES ('$nis','$nama','$kelas','$jurus')");
            if($input){
              echo"<script>window.location.assign('datasiswa.php')</script>";
            }else{
              echo"Gagal";
            }
          }
          ?>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

                    <footer class="page-footer">
        <div class="container">
          <div class="row">
            <div class="col l4 s12" style="width: 32.333333%;">
              <h5 class="white-text">Hubungi Kami</h5>
              <i class="material-icons" style="width: 24px; color: white;">add_locations</i><a style="color:white">SMK Negri 1 Ciomas | Jl.Laladon, Desa Laladon, Kecamatan Ciomas, Kab.Bogor.</a><br>
              <p><i class="material-icons" style="width: 27px; color: white;">website</i><a style="color:white">www.smkn1ciomas.sch.id </a> </p>
              <p><i class="material-icons" style="margin-left: -95px; width: 123px; color: white;">telephone</i><a style="color:white">08147919696594 </a></p>
            </div>
            <div class="col l4 s12" style="overflow: hidden; margin-left: 320px;">
              <h5 class="white-text">About</h5>
              <p class="grey-text text-lighten-4">Program ini memiliki fungsi untuk memudahkan siswa dalam pengabsenan dan memudahkan staf tata usaha dalam merekap absensi siswa.</p>
            </div>
          </div>
        </div>
       <div class="footer-copyright">
    <div class="container">
      © SMK Negeri 1 CIOMAS
    </div>
  </div>
        </div>
      </footer>

      <script>
        $(document).ready(function(){
          $('.modal').modal();
        });
      </script>

      <script type="text/javascript">
        $(document).ready(function() {
          $('select').material_select();
        });
      </script>

</body>
</html>