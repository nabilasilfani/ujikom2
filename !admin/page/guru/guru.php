<?php include '../../koneksi.php'; ?>
<?php include "../../kode_peminjaman.php";?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="../../../dist/css/materialize.min.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="../../../style.css">

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <!-- DataTables -->
  <link rel="stylesheet" href="../../../dist/plugin/datatables/dataTables.bootstrap.css">
  <script type="text/javascript" src="../../../dist/js/jquery.js"></script>
  <script type="text/javascript" src="../../../dist/js/materialize.min.js"></script>
  <script src="../../../dist/plugin/datatables/jquery.dataTables.js" type="text/javascript"></script>
  <script src="../../../dist/plugin/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

  <script type="text/javascript">
    $(function () {
      $('#table').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": true
      });
    });
  </script>

</head>
<body>
 <nav>
  <div class="container">
    <div class="nav-wrapper">
      <div class="brand-logo">INVENSCOO</div>
      <ul class="right hide-on-med-and-down">
        <li><a href="../../logout.php" style="color: white;"><i class="material-icons prefix">exit_to_app</i></a></li>
      </ul>
    </div>
  </div>
</nav>

<!-- Navbar goes here -->
<div class="container">
  <div class="row">
    <div class="col s3">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header" style="color:#039be5;"><a href="../../index.php"><i class="material-icons">dashboard</i>Dashboard</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">description</i>Data Master</div>
          <div class="collapsible-body"><a href="../barang/databarang.php">Data Barang</a></div>
          <div class="collapsible-body"><a href="../jenis/datajenis.php">Data Jenis</a></div>
          <div class="collapsible-body"><a href="../ruang/dataruang.php">Data Ruang</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">people</i>Data Pengguna</div>
          <div class="collapsible-body"><a href="../siswa/datasiswa.php">Data Siswa</a></div>
          <div class="collapsible-body"><a href="../guru/guru.php">Data Guru</a></div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">content_paste</i>Data Master</div>
          <div class="collapsible-body"><a href="../../datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div>
          <!-- <div class="collapsible-body"><a href="../../datapeminjaman.php?kode_peminjaman=<?php echo $kode; ?>">Data Peminjaman</a></div> -->
          <div class="collapsible-body"><a href="../../datapengembalian.php">Data pengembalian</a></div>
        </li>
      </ul>
    </div>

    <div class="col s12 m8 l8"> 
      <div class="card">
        <div class="card-heading"><h5>Identitas Guru</h5></div>
        <div class="card-content">
          <div class="responsive-table">
            <table id="table" class="striped">
             <div class="row">    
               <table id="table" class="striped">
                <a class="waves-effect waves-light btn cyan modal-trigger" href="tambah_guru.php">Tambah</a>
                <br>
                <br>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIP</th>
                    <th>Nama Guru</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 $no=1;
                 $query=mysql_query("SELECT * FROM guru");
                 while ($tampil=mysql_fetch_array($query)) {
                  echo "<tr>
                  <td>$no</td>
                  <td>$tampil[nip]</td>
                  <td>$tampil[nama_guru]</td>
                  <td>
                  <a href='edit_g.php?id=$tampil[id_guru]'><button class='btn btn-floating blue'><i class='material-icons'>edit</i></a></button> 
                  <a href='hapus_guru.php?id=$tampil[id_guru]'><button class='btn btn-floating red','delete-material-icons'><i class='material-icons'>delete</i></a></button>
                  </td>
                  </tr>";

                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>    
        </div>
      </div>
    </div>
<?php include "../../footer.php"; ?>
