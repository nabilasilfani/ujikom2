<!DOCTYPE html>
<html>
<head>
	<title>Help</title>
</head>
<body>
<div class="col-md-8">
			<!-- Default box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Manual Program</h3>
				</div>
				<div class="box-body">
					<table width="50%" border="1">
						<tr>
							<td>1.</td>
							<td>Login sebagai admin : username=admin , password=admin. Login sebagai operator : username=operator , password=operator</td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Pada halaman admin, data inventaris dapat dilihat keseluruhannya. Dan barang diluar jurusan atau bersifat umum seperti proyektor terdapat pada bagian barang dan data peminjaman lainnya. Pada tabel inventaris terdapat jenis yang berisi nama-nama barang inventaris yang sudah ada. </td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Peminjaman barang pada halaman admin dan operator, barang dapat dipinjam sesuai yang masuk ketika login. Jika ingin meminjam laptop atau barang lainnya maka bisa login sebagai admin,operator dan peminjam. pada data peminjaman terdapat form untuk di isi. Dan data yang sudah dipinjam akan masuk ke dalam fitur data pinjam yang berisi tabel data peminjaman dan dilanjutkan ke data pengembalian. Lalu jumlah barang inventaris akan berkurang setelah dipinjam.</td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Jika barang ingin dikembalikan, maka dapat dilihat pada tabel data pinjam dan bisa langsung klik button kembalikan, lalu barang tersebut akan masuk ke dalam tabel pengembalian dan jumlah barang inventaris akan menambah sesuai jumlah yang sudah dikembalikan tadi. </td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Pada fitur laporan terdapat data yang bisa dicetak melalui dan cetak pdf yaitu ada data pengembalian.</td>
						</tr>
						<tr>
							<td>8.</td>
							<td>Di dalam aplikasi inventaris ini, fitur backupdatabase masih belum berjalan dengan baik. Dan belum terdapat halaman login sebagai peminjam siswa</td>
						</tr>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
</body>
</html>