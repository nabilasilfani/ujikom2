<!DOCTYPE html>
<?php
include "../koneksi.php";
session_start();
if (isset($_SESSION['username'])){
	header("location:index_user.php");
}else{
	?><!DOCTYPE html>
	<html>
	<head>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>

		<form action="proseslogin2.php" method="POST" class="login-form">
			<div class="card-panel">
				<!-- <div class="row"> -->
					<div class="input-field col s12 center">
						<img src ="../img/logo.png" width="50%">
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">perm_identity</i>
						<input id="nip" type="text" name="nip" class="validate">
						<label for="nip">NIP</label>
					</div>
					<div class="input-field prefix">
						<input type="submit" name="submit" class="btn btn-block" value="Login">
					</div>
					<!-- </div> -->
				</div>
			</form>
			<!--Import jQuery before materialize.js-->
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/materialize.min.js"></script>
		</body>
		</html>
		<style type="text/css">
		html,
		body {
			height: 100%;
		}
		html {
			display: table;
			margin: auto;
		}
		body {
			display: table-cell;
			vertical-align: middle;
			background-image: url(img/body-bg.png);

		}
		.login-form{
			width: 300px;
		}
		.btn-block{
			width: 100%;
		}

	</style>
	<?php
}
?>


<script type="text/javascript">
	$(document).ready(function() {
		$('select').material_select();
	});
</script> 
